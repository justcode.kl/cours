<?php
$host = "localhost";
$dbname = "justcode-kl";

$name = $_POST['name'];
$email = $_POST['email'];
$faculty = $_POST['faculty'];
$capacity = $_POST['capacity'];
$age = $_POST['age'];
$pw = $_POST['password'];
$con_pw = $_POST['confirm_password'];
$level = $_POST['level'];

if($pw === $con_pw){
    $db = new PDO('mysql:host='.$host.';dbname='.$dbname, 'root', '', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
    
    $request = $db->prepare('SELECT * FROM users WHERE email=?');
    $request->execute(array($email));

    $count = $request->rowCount();

    if($count > 0){
        echo 'The email is alrady used';
    } else {
        $request = $db->prepare('INSERT INTO users (name, email, faculty, capacity, age, password, level) VALUES (?, ?, ?, ?, ?, ?, ?)');
        $request->execute(array($name, $email, $faculty, $capacity, $age, $pw, $level));

        echo 'Thank you very much for subscribing!';
    }
} else {
    echo 'Your password doesn\' match';
}


?>