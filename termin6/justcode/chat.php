<?php
require_once 'utils/dbConfig.php';
require_once 'utils/functions.php';

start_session();

if(!isConnected()){
    redirect_url('logIn.php');
}

var_dump($_POST);

if(!empty($_POST['message'])){
    $enregistre = $db->prepare('INSERT INTO messages (userId, message) VALUES (?,?)');
    $enregistre->execute([$_SESSION['user']['id'], $_POST['message']]);
}

$messages = $db->prepare('SELECT * FROM messages');
$messages->execute();

$senderUsers = $db->prepare('SELECT * FROM users WHERE id=?');


unset($_POST['message']);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mini Chat</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>
    <div class="card text-center">
        <div class="card-header">
            JustcCode -Chat: <?= $_SESSION['user']['name'] ?>
        </div>
        <div class="card-body">

            <?php foreach ($messages as $message): ?>
                <?php if($message['userId'] == $_SESSION['user']['id']): ?>

                    <div class="card text-white bg-primary mb-3" style="max-width: 18rem;">
                        <div class="card-header"><?= $_SESSION['user']['name'] ?></div>
                        <div class="card-body">
                            <h5 class="card-title"><?= $message['date'] ?></h5>
                            <p class="card-text"><?= $message['message'] ?></p>
                        </div>
                    </div>

                <?php else: 
                    $senderUsers->execute([$message['userId']]);
                    $senderUser = $senderUsers->fetch();
                    ?>

                    <div class="card text-white bg-secondary mb-3" style="max-width: 18rem;">
                        <div class="card-header"><?= $senderUser['name'] ?></div>
                        <div class="card-body">
                            <h5 class="card-title"><?= $message['date'] ?></h5>
                            <p class="card-text"><?= $message['message'] ?></p>
                        </div>
                    </div>

                <?php endif ?>
            <?php endforeach ?>
        </div>

        <div class="card-footer text-muted">
            <form action="chat.php" method="POST">
                <div class="input-group mb-3">
                        <input name="message" type="text" class="form-control" placeholder="Enter your message" aria-label="Enter your message" aria-describedby="button-addon2">
                        <div class="input-group-append">
                            <button class="btn btn-outline-secondary" type="submit" id="button-addon2">Send</button>
                        </div>
                </div>
            </form>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>