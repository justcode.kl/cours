<?php
require_once 'utils/dbConfig.php';
require_once 'utils/functions.php';

$error = null;

if(!empty($_POST['email'])){
    $pw = $_POST['password'];
    $email = $_POST['email'];

    $request = $db->prepare('SELECT * FROM users WHERE email=?');
    $request->execute(array($email));

    $count = $request->rowCount();
    start_session();

    if($count < 1){
        $error = 'The are no User with this email!!';
    } else{
        while($user = $request->fetch()){
            if( password_verify($pw, $user['password']) ){

                saveSession('id', $user['id']);
                saveSession('name', $user['name']);
                saveSession('email', $user['email']);

                var_dump($_SESSION);
                
                redirect_url('chat.php');
            } else {
                $error = 'Please check your password!!!';
            }
        }
    }
} else {
    start_session();
    unset($_SESSION['user']);
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>LogIn</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

</head>
<body>
    <?php if($error): ?>
        <div class="alert alert-danger" role="alert">
            <?= $error ?>
        </div>
    <?php endif ?>
    
    <form action="logIn.php" method="post">
        <label for="email">Your Email:</label>
        <input type="text" name="email" placeholder="enter your email" required>
        <label for="password">Password</label>
        <input type="password" name="password" placeholder="enter your password" required>
        <input type="submit" value="login">
    </form>

    
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>