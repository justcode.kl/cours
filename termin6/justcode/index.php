<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>JustCode</title>
</head>
<body>    
    <?php
        $adresse = '<b>dannick\'</b> <br>';
        echo $adresse;

        $adresse = "Stark";
        echo $adresse;

        $note = 4;
        if($note <= 4){
            echo "Sie haben bestanden";
        } else {
            echo "Sie haben nicht bestanden";
        }

        switch ($note) {
            case 1:
            case 2:
            case 3:
            case 4:
                echo "Sie haben bestanden";
                break;
            
            default:
                echo "Sie haben nicht bestanden";
                break;
        }

        for ($i=0; $i < 5; $i++) { 
            echo "<br>Wir machen PHP";
        }

        $a = 0;
        while ($a < 5) {
            echo "<br>Wir machen PHP2";
            $a++;
        }

        $list = [
            "name" => 5,
            "age" => 25,
            "prenom" => "Dannick"
        ];

        foreach ($list as $key => $value) {
            echo "<br>$key:$value";
        }

        include 'fonctions.php';

        $result = addition(5, 100);
        echo $result;

        echo "fin du code";
    ?>

    <a href="bonjour.php?nom=Dannick&prenom=Stark"> Djoss moi bonjour!!!</a>
</body>
</html>
