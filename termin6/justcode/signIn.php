<?php
require_once 'utils/dbConfig.php';
require_once 'utils/functions.php';

$error = null;

if(!empty($_POST['email'])){

    $name = $_POST['name'];
    $email = $_POST['email'];
    $faculty = $_POST['faculty'];
    $capacity = $_POST['capacity'];
    $age = $_POST['age'];
    $pw = $_POST['password'];
    $con_pw = $_POST['confirm_password'];
    $level = $_POST['level'];
    
    if($pw === $con_pw){
        
        $request = $db->prepare('SELECT * FROM users WHERE email=?');
        $request->execute(array($email));
    
        $count = $request->rowCount();
    
        if($count > 0){
            $error = 'The email is alrady used';
        } else {
            $hasedPass = password_hash($pw, PASSWORD_DEFAULT);
            $request = $db->prepare('INSERT INTO users (name, email, faculty, capacity, age, password, level) VALUES (?, ?, ?, ?, ?, ?, ?)');
            $request->execute(array($name, $email, $faculty, $capacity, $age, $hasedPass, $level));

            redirect_url('logIn.php');
        }
    } else {
        $error = 'Your password doesn\' match';
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SignIn</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

</head>
<body>
    <?php if($error): ?>
        <div class="alert alert-danger" role="alert">
            <?= $error ?>
        </div>
    <?php endif ?>

    <form action="signIn.php" method="POST">
        <label for="name">Name:</label>
        <input type="text" name="name" placeholder="enter your name" required>

        <label for="email">Mail:</label>
        <input type="email" name="email" placeholder="enter your email" required>

        <label for="faculty">Faculty:</label>
        <input type="text" name="faculty" placeholder="enter your faculty">

        <label for="level">Level:</label>
        <input type="text" name="level" placeholder="enter your level">

        <label for="capacity">Capacity:</label>
        <input type="text" name="capacity" placeholder="Html, css, php, ...">

        <label for="age">Age:</label>
        <input type="number" name="age" placeholder="enter your age">

        <label for="password">Password:</label>
        <input type="password" name="password" placeholder="enter your password" required>

        <label for="confirm_password">Confirmation:</label>
        <input type="password" name="confirm_password" placeholder="confirm your password" required>
        <input type="submit" value="SignIn">
    </form>

    
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>