<?php

function redirect_url($path){
    header('Location: '.$path);
}

function start_session(){
    if(session_status() == PHP_SESSION_NONE){
        session_start();
    }
}

function saveSession($key, $value){
    start_session();
    $_SESSION['user'][$key] = $value;
}

function isConnected(){
    start_session();
    return !empty($_SESSION['user']);
}


?>
